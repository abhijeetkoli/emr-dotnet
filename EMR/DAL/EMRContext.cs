namespace EMR.DAL
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using EMR.Models;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    public class EMRContext : DbContext
    {
        // Your context has been configured to use a 'EMRContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'EMR.DAL.EMRContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'EMRContext' 
        // connection string in the application configuration file.
        public EMRContext()
            : base("name=EMRContext")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<Speciality> Specialities { get; set; }
        public virtual DbSet<Patient> Patients { get; set; }
        public virtual DbSet<Physician> Physicians { get; set; }
        public virtual DbSet<Appointment> Appointments { get; set; }
        public virtual DbSet<Visit> Visits { get; set; }

        public virtual ObjectResult<PhysicianReport_Result> PhysicianReport(Nullable<int> physicianId)
        {
            var physicianIdParameter = physicianId.HasValue ?
                new ObjectParameter("physicianId", physicianId) :
                new ObjectParameter("physicianId", typeof(int));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PhysicianReport_Result>("PhysicianReport", physicianIdParameter);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Physician>()
            .HasMany(p => p.Patients)
            .WithRequired()
            .HasForeignKey(c => c.PhysicianId)
            .WillCascadeOnDelete(false);
        }

    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}

}