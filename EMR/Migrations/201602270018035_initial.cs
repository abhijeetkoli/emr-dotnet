namespace EMR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Appointments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartsAt = c.DateTime(nullable: false),
                        EndsAt = c.DateTime(nullable: false),
                        PatientId = c.Int(nullable: false),
                        PhysicianId = c.Int(nullable: false),
                        ChiefComplaint = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patients", t => t.PatientId, cascadeDelete: true)
                .ForeignKey("dbo.Physicians", t => t.PhysicianId, cascadeDelete: true)
                .Index(t => t.PatientId)
                .Index(t => t.PhysicianId);
            
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        City = c.String(),
                        Zip = c.String(),
                        State = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        PhysicianId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Physicians", t => t.PhysicianId)
                .Index(t => t.PhysicianId);
            
            CreateTable(
                "dbo.Physicians",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        SpecialityId = c.Int(nullable: false),
                        RegistrationNumber = c.String(),
                        RegistrationState = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Specialities", t => t.SpecialityId, cascadeDelete: true)
                .Index(t => t.SpecialityId);
            
            CreateTable(
                "dbo.Specialities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Visits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AppointmentId = c.Int(nullable: false),
                        Note = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Appointments", t => t.AppointmentId, cascadeDelete: true)
                .Index(t => t.AppointmentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Visits", "AppointmentId", "dbo.Appointments");
            DropForeignKey("dbo.Physicians", "SpecialityId", "dbo.Specialities");
            DropForeignKey("dbo.Patients", "PhysicianId", "dbo.Physicians");
            DropForeignKey("dbo.Appointments", "PhysicianId", "dbo.Physicians");
            DropForeignKey("dbo.Appointments", "PatientId", "dbo.Patients");
            DropIndex("dbo.Visits", new[] { "AppointmentId" });
            DropIndex("dbo.Physicians", new[] { "SpecialityId" });
            DropIndex("dbo.Patients", new[] { "PhysicianId" });
            DropIndex("dbo.Appointments", new[] { "PhysicianId" });
            DropIndex("dbo.Appointments", new[] { "PatientId" });
            DropTable("dbo.Visits");
            DropTable("dbo.Specialities");
            DropTable("dbo.Physicians");
            DropTable("dbo.Patients");
            DropTable("dbo.Appointments");
        }
    }
}
