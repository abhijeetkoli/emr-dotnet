namespace EMR.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using EMR.Models;
    using System.Reflection;
    using System.IO;
    using System.Text;
    using CsvHelper;
    internal sealed class Configuration : DbMigrationsConfiguration<EMR.DAL.EMRContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EMR.DAL.EMRContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //Import Speciality CSV
            Assembly assembly = Assembly.GetExecutingAssembly();
            String specialityCsvName = "EMR.Domain.SeedData.specialites.csv";
            using (Stream stream = assembly.GetManifestResourceStream(specialityCsvName))
            {
                using(StreamReader reader = new StreamReader(stream))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;

                    while (csvReader.Read())
                    {
                        var specialityCode = csvReader.GetField<string>("Code");
                        var specialityName = csvReader.GetField<string>("Name");

                        context.Specialities.AddOrUpdate(
                            new Speciality { Code = specialityCode, Name = specialityName }
                            );
                    }

                }

            }

        }
    }
}
