﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMR.Models
{
    public class Appointment
    {
        public int Id { get; set; }
        public DateTime StartsAt { get; set; }
        public DateTime EndsAt { get; set; }
        public int PatientId { get; set; }
        public int PhysicianId { get; set; }
        public string ChiefComplaint { get; set; }

        public virtual Patient Patient { get; set; }
        public virtual Physician Phisician { get; set; }
    }
}
