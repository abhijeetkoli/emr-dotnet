﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EMR.Models
{
    public class Physician
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int SpecialityId { get; set; }
        public string RegistrationNumber { get; set; }
        public string RegistrationState { get; set; }

        public virtual Speciality Speciality { get; set; }
        public virtual ObservableListSource<Patient> Patients { get; set; }
        public virtual ObservableListSource<Appointment> Appointments { get; set; }
    }
}
