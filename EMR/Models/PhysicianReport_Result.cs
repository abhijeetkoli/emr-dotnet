﻿namespace EMR.Models
{
    using System;

    public partial class PhysicianReport_Result
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}