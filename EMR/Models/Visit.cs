﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMR.Models
{
    public class Visit
    {
        public int Id { get; set; }
        public int AppointmentId { get; set; }
        public string Note { get; set; }

        public virtual Appointment Appointment { get; set; }
    }
}
