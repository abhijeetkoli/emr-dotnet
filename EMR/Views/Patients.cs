﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using EMR.DAL;
using EMR.Models;

namespace EMR.Views
{
    public partial class Patients : Form
    {
        EMRContext _context;
        List<Physician> _pcps;

        public Patients()
        {
            InitializeComponent();
        }

        private void Patients_Load(object sender, EventArgs e)
        {
            _context = new EMRContext();

            _pcps = _context.Physicians.OrderBy(p => p.FirstName).ToList();

            _context.Patients.Load();
            patientBindingSource.DataSource = _context.Patients.Local.ToBindingList();

            FillCombos();

        }

        private void FillCombos()
        {
            pcpCombo.DisplayMember = "FirstName";
            pcpCombo.ValueMember = "Id";
            pcpCombo.DataSource = _pcps;
            pcpCombo.DataBindings.Add(new Binding("SelectedItem", patientBindingSource, "Physician"));
        }

        private void UpdatePatient(object sender, EventArgs e)
        {
            patientBindingSource.EndEdit();

            try
            {
                _context.SaveChanges();
            }
            catch(Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
           
        }

        private void pcpCombo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
