﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using EMR.DAL;
using EMR.Models;

namespace EMR.Views
{
    public partial class Physicians : Form
    {
        EMRContext _context;

        List<Physician> _physicians;
        List<Patient> _patients;
        List<Speciality> _specialities;


        public Physicians()
        {
            InitializeComponent();
        }

        private void Physicians_Load(object sender, EventArgs e)
        {
            _context = new EMRContext();

            _patients = _context.Patients.OrderBy(p => p.FirstName).ToList();
            _specialities = _context.Specialities.OrderBy(s => s.Name).ToList();

            patientBindingSource.DataSource = _patients;
            specialityBindingSource.DataSource = _specialities;

            _context.Physicians.Load();
            physicianBindingSource.DataSource = _context.Physicians.Local.ToBindingList();

            FillCombos();
            FillListView();
        }

        private void btnSavePhysician_Click(object sender, EventArgs e)
        {
            physicianBindingSource.EndEdit();

            try
            {
                _context.SaveChanges();
            } catch(Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
            
        }

        private void FillCombos()
        {
            //physician speciality dropdown
            specialitiesCombo.DisplayMember = "Name";
            specialitiesCombo.ValueMember = "Id";
            specialitiesCombo.DataSource = _specialities;
            specialitiesCombo.DataBindings.Add(new Binding("SelectedItem", physicianBindingSource, "Speciality"));

            //appointments grid patient dropdown
            DataGridViewComboBoxColumn patientColumn; 
            patientColumn = (DataGridViewComboBoxColumn)dataGridView1.Columns["patientId"];
            patientColumn.DataSource = _patients;
            patientColumn.ValueType = typeof(int);
            patientColumn.DataPropertyName = "PatientId";
            patientColumn.DisplayMember = "FirstName";
            patientColumn.ValueMember = "Id";       

        }

        private void FillListView()
        {
            foreach(Physician physician in _context.Physicians)
            {
                ListViewItem physicianItem = new ListViewItem();

                physicianItem.Text = physician.FirstName + " " + physician.LastName;
                physicianItem.Tag = physician.Id;

                listView1.Items.Add(physicianItem);
            }
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            var physician = ((ListView)sender).SelectedItems[0];
            var id = int.Parse(physician.Tag.ToString());

            if(physicianBindingSource.SupportsSearching)
            {
                var physicianFound = physicianBindingSource.Find("Id", id);
                physicianBindingSource.Position = physicianFound;
            }

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void physicianBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            var p = (Physician)physicianBindingSource.Current;
            //var appointmentsCount = p.Appointments.Count();
            //var patientsCount = p.Patients.Count();              
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            /*if (e.Context == (DataGridViewDataErrorContexts.Formatting | DataGridViewDataErrorContexts.PreferredSize))
            {
                e.ThrowException = false;
            }*/

        }
    }
}
